package com.example.akhil.locationbased;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.parse.ParseGeoPoint;

/**
 * Created by Akhil on 6/10/2015.
 */
public class LaunchApp extends Activity {

    Button launch;
    GPSTracker gps;
    ParseGeoPoint userLocation;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page);
        gps = new GPSTracker(LaunchApp.this);
        if(gps.canGetLocation()){
            double lattitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            userLocation = new ParseGeoPoint(lattitude,longitude);
            //Toast.makeText(getApplicationContext(), "Your location is -\nLat: " + userLocation.getLatitude() + " -\nLong: " + userLocation.getLongitude(), Toast.LENGTH_LONG).show();
        }
        launch = (Button) findViewById(R.id.buttonLaunch);
        launch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LaunchApp.this,MainActivity.class);
                startActivity(intent);
            }
        });
        // Retrieve data from MainActivity on item click event
    }
}
