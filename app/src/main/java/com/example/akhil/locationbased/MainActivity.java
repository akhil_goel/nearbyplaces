package com.example.akhil.locationbased;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {
    // Declare Variables
    ListView listview;
    List<ParseObject> ob;
    //ProgressDialog mProgressDialog;
    private List<Company> companylist = null;
    GPSTracker gps;
    //Button btnShowLocation,btnSort;
    ParseGeoPoint userLocation;
    ListSortAdapter adapterSort;
    //ParseFile file,file1;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the view from listview_main.xml
        Parse.initialize(this, "fGWROhHOzMfqw2kvJBPLo2hiTCEjCPGCHJKHRXEE", "GnkjekT07hXop2GchDISaPu1jx1ZukJwgSxU6Hzs");
        // Execute RemoteDataTask AsyncTask
        gps = new GPSTracker(MainActivity.this);
            if(gps.canGetLocation()){
                double lattitude = gps.getLatitude();
                double longitude = gps.getLongitude();
                userLocation = new ParseGeoPoint(lattitude,longitude);
                //Toast.makeText(getApplicationContext(), "Your location is -\nLat: " + userLocation.getLatitude() + " -\nLong: " + userLocation.getLongitude(), Toast.LENGTH_LONG).show();
                new Sort().execute();
                setContentView(R.layout.activity_main);
            }
        //new RemoteDataTask().execute();
        //btnShowLocation = (Button) findViewById(R.id.show_location);
        //btnSort = (Button) findViewById(R.id.sort_list);
        /*btnShowLocation.setOnClickListener(new View.OnClickListener() {
          //  @Override
            //public void onClick(View view) {
             gps = new GPSTracker(MainActivity.this);
            if(gps.canGetLocation()){
                double lattitude = gps.getLatitude();
                double longitude = gps.getLongitude();
                userLocation = new ParseGeoPoint(lattitude,longitude);
                Toast.makeText(getApplicationContext(), "Your location is -\nLat: " + userLocation.getLatitude() + " -\nLong: " + userLocation.getLongitude(), Toast.LENGTH_LONG).show();
            }
           // }
        });*/
        /*btnSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Sort().execute();
            }
        });*/
    }

    private class Sort extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            /*mProgressDialog = new ProgressDialog(MainActivity.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Companies");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Create the array
            companylist = new ArrayList<Company>();
            try {
                // Locate the class table named "Country" in Parse.com
                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("companies");
                // Locate the column named "ranknum" in Parse.com and order list
                // by ascending
                query.whereNear("location",userLocation);
                ob = query.find();
                for (ParseObject company : ob) {
                    Company map = new Company();
                    map.setName((String) company.get("companyName"));
                    map.setShort((String) company.get("shortDesc"));
                    map.setDesc((String) company.get("Description"));
                    map.setAddress((String)company.get("Address"));
                    map.setNumber((Integer)company.get("phoneNumber"));
                    Double dist = userLocation.distanceInKilometersTo((ParseGeoPoint)company.get("location"));
                    map.setIcon((ParseFile) company.get("Icon"));
                    map.setDistance(dist);
                    companylist.add(map);
                }
            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Locate the listview in listview_main.xml
            listview = (ListView) findViewById(R.id.listView);
            // Pass the results into ListViewAdapter.java
            adapterSort = new ListSortAdapter(MainActivity.this, companylist);
            // Binds the Adapter to the ListView
            listview.setAdapter(adapterSort);
            // Close the progressdialog
            //mProgressDialog.dismiss();
        }
    }
}
