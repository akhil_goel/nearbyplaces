package com.example.akhil.locationbased;

/**
 * Created by Akhil on 6/6/2015.
 */
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;

import java.util.ArrayList;
import java.util.List;

public class ListSortAdapter extends BaseAdapter {

    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<Company> companylist = null;
    private ArrayList<Company> arraylist;

    public ListSortAdapter(Context context,
                           List<Company> companylist) {
        mContext = context;
        this.companylist = companylist;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<Company>();
        this.arraylist.addAll(companylist);
    }

    public class ViewHolder {
        TextView companyname;
        TextView shortDesc;
        TextView distance;
        ImageView icon;
    }

    @Override
    public int getCount() {
        return companylist.size();
    }

    @Override
    public Company getItem(int position) {
        return companylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listview_item, null);
            // Locate the TextViews in listview_item.xml
            holder.companyname = (TextView) view.findViewById(R.id.companyNameMain);
            holder.shortDesc = (TextView) view.findViewById(R.id.shortDesc);
            holder.distance = (TextView) view.findViewById(R.id.textDist);
            holder.icon = (ImageView) view.findViewById(R.id.companyImage);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.companyname.setText(companylist.get(position).getName());
        holder.shortDesc.setText(companylist.get(position).getShort());
        String one = String.format("%.5g%n", companylist.get(position).getDistance());
        holder.distance.setText(one + " km");
        ParseFile file = companylist.get(position).getIcon();
        file.getDataInBackground(new GetDataCallback() {
            @Override
            public void done(byte[] data, ParseException e) {
                if(e == null){
                    Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                    holder.icon.setImageBitmap(bmp);
                }
                else{
                    Log.d("test", "There is a problem showing images");
                }

            }
        });
        // Listen for ListView Item Click
        view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Send single item click data to SingleItemView Class
                Intent intent = new Intent(mContext, SingleItemView.class);
                // Pass all data rank
                intent.putExtra("companyName", (companylist.get(position).getName()));
                // Pass all data country
                // Start SingleItemView Class
                mContext.startActivity(intent);
            }
        });

        return view;
    }
}