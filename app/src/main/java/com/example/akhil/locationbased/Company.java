package com.example.akhil.locationbased;

import com.parse.ParseFile;
import com.parse.ParseGeoPoint;

/**
 * Created by Akhil on 6/6/2015.
 */
public class Company {
    private String name;
    private String shortDesc;
    private String desc;
    private String address;
    private Integer number;
    private Double distance;
    private ParseGeoPoint loc;
    private ParseFile icon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShort() {
        return shortDesc;
    }

    public void setShort(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(Double dist) {
        this.distance = dist;
    }

    public ParseGeoPoint getLocation() {
        return loc;
    }

    public void setLocation(ParseGeoPoint location) {
        this.loc = location;
    }

    public String getAddress(){ return address; }

    public void setAddress(String add){ this.address = add;}

    public ParseFile getIcon(){ return icon; }

    public void setIcon(ParseFile icon){ this.icon = icon;}

    public Integer getNumber(){ return number; }

    public void setNumber(Integer num){ this.number = num;}
}
