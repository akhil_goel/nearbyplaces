package com.example.akhil.locationbased;

/**
 * Created by Akhil on 6/6/2015.
 */
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import java.util.ArrayList;
import java.util.List;

public class SingleItemView extends Activity {
    // Declare Variables
    TextView txtcompany;
    TextView txtFullDesc;
    TextView txtAddress;
    TextView txtNumber;
    String companyName;
    String fullDesc;
    String address;
    Integer number;
    ParseFile file;
    ImageView image;
    Bitmap bmp;
    List<ParseObject> ob;
    private List<Company> companylist = null;
    Button btnEdit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        companyName = i.getStringExtra("companyName");
        address = i.getStringExtra("address");
        new Sort().execute();
        setContentView(R.layout.singleitemview);
        btnEdit = (Button) findViewById(R.id.buttonEdit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SingleItemView.this,EditCompany.class);
                intent.putExtra("companyName",companyName);
                startActivity(intent);
            }
        });
    }
    private class Sort extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            /*mProgressDialog = new ProgressDialog(MainActivity.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Companies");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Create the array
            companylist = new ArrayList<Company>();
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("companies");
            query.whereEqualTo("companyName", companyName);
            try {
                ob = query.find();
                for(ParseObject company : ob){
                    address = (String) company.get("Address");
                    number = (Integer) company.get("phoneNumber");
                    fullDesc = (String) company.get("Description");
                    file = (ParseFile) company.get("Icon");
                    file.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] data, ParseException e) {
                            bmp = BitmapFactory.decodeByteArray(data,0,data.length);
                            image = (ImageView) findViewById(R.id.companyIcon);
                            image.setImageBitmap(bmp);
                        }
                    });
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            txtcompany = (TextView) findViewById(R.id.txtcompanyName);
            txtAddress = (TextView) findViewById(R.id.txtAddress);
            txtFullDesc = (TextView) findViewById(R.id.txtfullDesc);
            txtNumber = (TextView) findViewById(R.id.txtNumber);
            txtcompany.setText(companyName);
            txtAddress.setText(address);
            txtFullDesc.setText(fullDesc);
            txtNumber.setText("+91 "+number);
        }
    }
}