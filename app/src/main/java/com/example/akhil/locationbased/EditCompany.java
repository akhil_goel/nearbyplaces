package com.example.akhil.locationbased;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Akhil on 6/10/2015.
 */
public class EditCompany extends Activity {

    String companyName,newcompanyName;
    String Address,newAddress;
    Integer number,newnumber;
    ParseGeoPoint loc,newloc,userLocation;
    GPSTracker gps;
    Button btnLocation,btnUpdate;
    List<ParseObject> ob;
    private List<Company> companylist = null;
    EditText companyname,address,phoneNumber;
    String objectId;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        companyName = i.getStringExtra("companyName");
        new getData().execute();
        setContentView(R.layout.edit_company);
        btnLocation = (Button) findViewById(R.id.buttonLocation);
        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gps = new GPSTracker(EditCompany.this);
                if(gps.canGetLocation()) {
                    double lattitude = gps.getLatitude();
                    double longitude = gps.getLongitude();
                    userLocation = new ParseGeoPoint(lattitude, longitude);
                }
                loc = userLocation;
            }
        });
        btnUpdate = (Button) findViewById(R.id.buttonUpdate);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                companyName = companyname.getText().toString();
                Address = address.getText().toString();
                number = Integer.valueOf(phoneNumber.getText().toString());
                new setData().execute();
            }
        });
    }
    private class getData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            /*mProgressDialog = new ProgressDialog(MainActivity.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Companies");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Create the array
            companylist = new ArrayList<Company>();
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("companies");
            query.whereEqualTo("companyName", companyName);
            try {
                ob = query.find();
                for(ParseObject company : ob){
                    Address = (String) company.get("Address");
                    number = (Integer) company.get("phoneNumber");
                    loc = (ParseGeoPoint) company.get("location");
                    objectId = (String) company.getObjectId();
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            companyname = (EditText) findViewById(R.id.editCompanyName);
            address = (EditText) findViewById(R.id.editAddress);
            phoneNumber = (EditText) findViewById(R.id.editNumber);
            companyname.setText(companyName);
            address.setText(Address);
            phoneNumber.setText(""+number);
        }
    }
    private class setData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Create a progressdialog
            /*mProgressDialog = new ProgressDialog(MainActivity.this);
            // Set progressdialog title
            mProgressDialog.setTitle("Companies");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Create the array
            companylist = new ArrayList<Company>();
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("companies");
            query.whereEqualTo("objectId",objectId);
            try {
                ob = query.find();
                for(ParseObject company : ob){
                    company.put("companyName",companyName);
                    company.put("Address",Address);
                    company.put("phoneNumber",number);
                    company.put("location",loc);
                    company.saveInBackground();
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Intent intent = new Intent(EditCompany.this,SingleItemView.class);
            intent.putExtra("companyName",companyName);
            startActivity(intent);
        }
    }
}
